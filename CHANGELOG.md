## [3.4.1](https://gitlab.com/to-be-continuous/bash/compare/3.4.0...3.4.1) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([c791d1b](https://gitlab.com/to-be-continuous/bash/commit/c791d1b80c9ee3b29a30b1bb0141b26ad160fd9d))

# [3.4.0](https://gitlab.com/to-be-continuous/bash/compare/3.3.0...3.4.0) (2024-1-27)


### Features

* migrate to GitLab CI/CD component ([c85e02f](https://gitlab.com/to-be-continuous/bash/commit/c85e02f06462a35f09abf725ef22b6b4b031ea2a))

# [3.3.0](https://gitlab.com/to-be-continuous/bash/compare/3.2.1...3.3.0) (2023-12-8)


### Features

* use centralized tracking image (gitlab.com) ([f727acf](https://gitlab.com/to-be-continuous/bash/commit/f727acfa45b56094718d7eb4ab78747d4108ec23))

## [3.2.1](https://gitlab.com/to-be-continuous/bash/compare/3.2.0...3.2.1) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([47fecf3](https://gitlab.com/to-be-continuous/bash/commit/47fecf3e06bc86036f58899660a06cf936a94c26))

# [3.2.0](https://gitlab.com/to-be-continuous/bash/compare/3.1.2...3.2.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([7257a5f](https://gitlab.com/to-be-continuous/bash/commit/7257a5fc7706664b4f619f64a98983337b48c193))

## [3.1.2](https://gitlab.com/to-be-continuous/bash/compare/3.1.1...3.1.2) (2023-02-22)


### Bug Fixes

* bats report output ([310a343](https://gitlab.com/to-be-continuous/bash/commit/310a343aafdbaf41aae20e6db6b55e941364d5b5))

## [3.1.1](https://gitlab.com/to-be-continuous/bash/compare/3.1.0...3.1.1) (2023-01-27)


### Bug Fixes

* Add registry name in all Docker images ([aa38096](https://gitlab.com/to-be-continuous/bash/commit/aa3809644a1bf0ee2fccf7bc3626ef5afb6c0080))

# [3.1.0](https://gitlab.com/to-be-continuous/bash/compare/3.0.1...3.1.0) (2022-10-04)


### Features

* document reports ([27e103c](https://gitlab.com/to-be-continuous/bash/commit/27e103c2148241bff4f933c130eba7f91a37310a))

## [3.0.1](https://gitlab.com/to-be-continuous/bash/compare/3.0.0...3.0.1) (2022-08-10)


### Bug Fixes

* add missing branch variables ([75c9d26](https://gitlab.com/to-be-continuous/bash/commit/75c9d264e68a7dc10c44e38f07702dc7bc7f506c))

# [3.0.0](https://gitlab.com/to-be-continuous/bash/compare/2.1.1...3.0.0) (2022-08-05)


### Features

* adaptive pipeline ([543d03a](https://gitlab.com/to-be-continuous/bash/commit/543d03aab8471bf722117c14d7c0a084107fb020))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

## [2.1.1](https://gitlab.com/to-be-continuous/bash/compare/2.1.0...2.1.1) (2022-05-06)


### Bug Fixes

* **rules:** implement positive match rule for shellcheck ([0aa727d](https://gitlab.com/to-be-continuous/bash/commit/0aa727d19e0ad243564ace568e885a3a15e689f9))

# [2.1.0](https://gitlab.com/to-be-continuous/bash/compare/2.0.1...2.1.0) (2022-05-01)


### Features

* configurable tracking image ([d204ccc](https://gitlab.com/to-be-continuous/bash/commit/d204cccb77c7c257d7655d2df962b30cbbd07511))

## [2.0.1](https://gitlab.com/to-be-continuous/bash/compare/2.0.0...2.0.1) (2021-10-04)


### Bug Fixes

* update tag version in script ([a26cb1d](https://gitlab.com/to-be-continuous/bash/commit/a26cb1d3399ff14124f7e57707e83ab1e55a6118))

## [2.0.0](https://gitlab.com/to-be-continuous/bash/compare/1.0.0...2.0.0) (2021-09-02)

### Features

* Change boolean variable behaviour ([803eac3](https://gitlab.com/to-be-continuous/bash/commit/803eac3ddd20d039845836105a721191cdba7f21))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## 1.0.0 (2021-06-10)

### Features

* initial release ([a925c72](https://gitlab.com/to-be-continuous/bash/commit/a925c722df2df4d2eb986b5eb74c57652b70e96c))
